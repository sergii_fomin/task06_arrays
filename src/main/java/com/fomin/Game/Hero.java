package com.fomin.Game;

import java.util.Random;

public class Hero {
    private final int initHeroStrength = 25;
    private int heroStrength;

    // For first opening door strength of hero is set by default
    public void setHeroStrength() {
        this.heroStrength = initHeroStrength;
    }

    // For other opening
    private void setHeroStrength(int heroStrength) {
        this.heroStrength += heroStrength;
    }

    // If behind the door there is an artifact
    public void findArtifact(){
        Random rnd = new Random();
        System.out.println("Strength hero before get power from artifact = " + getHeroStrength() + ".");
        setHeroStrength(10 + rnd.nextInt(71));
        System.out.println("Strength hero after get power from artifact " + this.heroStrength + ".");
    }

    public int getHeroStrength(){
        return heroStrength;
    }
}
