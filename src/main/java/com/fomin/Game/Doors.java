package com.fomin.Game;

import java.util.Random;
import java.util.Scanner;
import java.util.ArrayList;

public class Doors {

    private Hero hero = new Hero();
    private Monster monster = new Monster();
    private ArrayList<Integer> openDoors = new ArrayList<>(10);
    private ArrayList<String> closeDoors = new ArrayList<>();

    private void setCloseDoor() {
        for (int i = 0; i < 10; i++) {
            closeDoors.add(Integer.toString(i + 1));
        }
    }

    public void getDoor() {

        int pick;                                           // number of door
        String[] choice = {"magic artifact", "monster"};    // what you'll see behind the doors

        if (Main.first) {
            setCloseDoor();
        }

        System.out.println("Which door do you choose?");
        System.out.println("You may to open the next door: " + closeDoors.toString());
        Scanner scn = new Scanner(System.in);
        pick = scn.nextInt();

        // You must input value from 1 to 10
        int permit = 0;
        do {
            if (pick >= 1 && pick <= 10)
                permit = 1;
            while (pick < 1 || pick > 10) {
                permit = 0;
                System.out.println("Oops, it seems you made a mistake with door, let's try once again.\n");
                System.out.println("Which door do you choose?");
                System.out.println("You may to open the next door: " + closeDoors.toString());
                pick = scn.nextInt();
            }
            // If door have opened you can't open it twice
            while (openDoors.contains(pick)) {
                permit = 0;
                System.out.println(pick + " door have been opened, please open other door\n");
                System.out.println("Which door do you choose?");
                System.out.println("You may to open the next door: " + closeDoors.toString());
                pick = scn.nextInt();
            }
        } while (permit == 0);

        openDoors.add(pick);                                    // add opened door in array
        closeDoors.remove(Integer.toString(pick));              // delete opened door from array

        // what will be from the door?
        Random rnd = new Random();
        int temp = rnd.nextInt(2);
        System.out.println("You opened door number " + pick + ". From this door you see a " + choice[temp]);

        // If this a magic artifact
        if (temp == 0) {
            // If you opened first door
            if (Main.first) {
                hero.setHeroStrength();
            }
            // For not first time
            hero.findArtifact();
        }

        // If this a monster
        if (temp == 1) {
            // If you opened first door
            if (Main.first) {
                hero.setHeroStrength();
            }
            // For not first time
            monster.findMonster();
            System.out.println("Strength hero = " + hero.getHeroStrength() + ". Strength monster = " + monster.getMonsterStrength());
            fight();
            System.out.println("Strength hero after fighting = " + fight());
            Monster.winMonster(fight());
        }
        System.out.println("");
    }

    private int fight() {
        return hero.getHeroStrength() - monster.getMonsterStrength();
    }
}