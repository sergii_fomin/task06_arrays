package com.fomin.Game;

import java.util.Random;

public class Monster {
    private int monsterStrength;

    private void setMonsterStrength(int monsterStrength) {
        this.monsterStrength = monsterStrength;
    }

    // If behind the door there is an monster
    public void  findMonster(){
        Random rnd = new Random();
        setMonsterStrength(5 + rnd.nextInt(96));
    }

    public int getMonsterStrength() {
        return monsterStrength;
    }

    // If Strength of monster bigger than hero, you lose
    public static void winMonster(int heroStrength){
        if(heroStrength < 0){
            System.out.println("You lose");
            System.exit(100);
        }
    }
}
