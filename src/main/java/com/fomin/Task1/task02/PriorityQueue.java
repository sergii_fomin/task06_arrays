package com.fomin.Task1.task02;

import java.util.*;

public class PriorityQueue<E> extends AbstractQueue<E> {
    private List<E> list = new ArrayList<>();

    private Comparator<E> productComparator;

    public PriorityQueue() {}

    public PriorityQueue(Comparator<E> productComparator) {
        this.productComparator = productComparator;
    }

    @Override
    public Iterator<E> iterator() {
        return new ProductIterator();
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public boolean offer(E e) {
        if (e == null) {
            throw new NullPointerException();
        }
        list.add(e);
        return true;
    }

    @Override
    public E poll() {
        E element = null;
        if (productComparator == null) {
            element = list.get(0);
            list.remove(element);
            return element;
        }
        E previous = list.get(0);
        if (list.size() == 1) {
            element = list.get(0);
            list.remove(element);
            return element;
        }
        for (int i = 0; i < list.size(); ++i) {
            int result = productComparator.compare(previous, list.get(i));
            if (result < 0) element = previous;
            else if (result > 0) element = list.get(i);
            previous = list.get(i);
        }
        list.remove(element);
        return element;
    }

    @Override
    public E peek() {
        return list.get(0);
    }

    private class ProductIterator implements Iterator<E> {
        private int position = 0;

        @Override
        public boolean hasNext() {
            return position < list.size();
        }

        @Override
        public E next() {
            if (hasNext()) {
                return list.get(position++);
            } else {
                return null;
            }
        }
    }
}
