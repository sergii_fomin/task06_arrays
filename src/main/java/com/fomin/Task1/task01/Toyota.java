package com.fomin.Task1.task01;

public class Toyota extends Car {
    public Toyota(String name, int price, int releaseYear) {
        super(name, price, releaseYear);
    }

    @Override
    public String toString() {
        return "Toyota{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", releaseYear=" + releaseYear +
                '}';
    }
}
