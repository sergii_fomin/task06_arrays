package com.fomin.Task1.task01;

public class Honda extends Car {
    public Honda(String name, int price, int releaseYear) {
        super(name, price, releaseYear);
    }

    @Override
    public String toString() {
        return "Honda{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", releaseYear=" + releaseYear +
                '}';
    }
}
