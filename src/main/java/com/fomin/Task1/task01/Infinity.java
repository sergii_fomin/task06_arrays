package com.fomin.Task1.task01;

public class Infinity extends Car {
    public Infinity(String name, int price, int releaseYear) {
        super(name, price, releaseYear);
    }

    @Override
    public String toString() {
        return "Infinity{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", releaseYear=" + releaseYear +
                '}';
    }
}
