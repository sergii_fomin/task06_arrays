package com.fomin.Task1.task01;

import java.util.ArrayList;
import java.util.List;

public class CarContainer<T extends Car> {

    private List<? super Car> carList = new ArrayList<>();

    public void add(T product) {
        carList.add(product);
    }

    public void remove(T product) {
        carList.remove(product);
    }

    public void addAll(List<? extends T> list) {
        carList.addAll(list);
    }

    public List<? super T> getAll() {
        return carList;
    }

    public void printAll(List<? super T> products) {
        products.forEach(System.out::println);
    }

}
