package com.fomin.Task1;

import com.fomin.Task1.task01.Infinity;
import com.fomin.Task1.task01.Toyota;
import com.fomin.Task1.task01.Honda;
import com.fomin.Task1.task01.Car;
import com.fomin.Task1.task01.CarContainer;
import com.fomin.Task1.task02.PriorityQueue;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class Application {
    public static void main(String[] args) {
        CarContainer<Car> carContainer = new CarContainer<>();
        carContainer.add(new Infinity("QX80", 1794530, 2019));
        carContainer.add(new Toyota("Land Cruiser 200", 2119880, 2019));
        carContainer.add(new Honda("CR-V", 927250, 2019));
        List<Car> products = new ArrayList<Car>() {
            {
                add(new Infinity("QX60",     1022690, 2019));
                add(new Infinity("QX50", 1029340, 2019));
                add(new Toyota("Highlander", 1263340, 2019));
                add(new Toyota("RAV4", 997564, 2019));
                add(new Honda("HR-V", 623200, 2019));
                add(new Honda("Pilot", 1279055, 2019));
            }
        };
//        productContainer.printAll(productContainer.getAll());
        carContainer.addAll(products);

        PriorityQueue<Car> priorityQueue = new PriorityQueue<>(new ProductComparator());
        priorityQueue.addAll(products);
        Iterator it = priorityQueue.iterator();
        while(it.hasNext()) {
//            System.out.println(it.next());
            System.out.println(priorityQueue.poll().getName());
        }

    }
    static class ProductComparator implements Comparator<Car> {
        @Override
        public int compare(Car o1, Car o2) {
            if (o1.getReleaseYear() < o2.getReleaseYear()) {
                return -1;
            } else if(o1.getReleaseYear() > o2.getReleaseYear()) {
                return 1;
            }
            return 0;
        }
    }
}