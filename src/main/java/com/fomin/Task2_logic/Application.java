package com.fomin.Task2_logic;

import java.util.*;

public class Application {

    public static void main(String[] args) {
        createArrayWithCommonValues();// Calls method for Array creating with common values
        createArrayWithUniqueValues(); // Calls method for Array creating with unique values
        removeDuplicatesInArray(); // Calls method which removes duplicates(more than 2 times) form array
    }

    private static void createArrayWithCommonValues() {
        int[] firstArray = generateRandomArray(10, 20);
        int[] secondArray = generateRandomArray(20, 10);

        System.out.println("First Array elements:");
        printArray(firstArray);
        System.out.println("Second Array elements:");
        printArray(secondArray);
        // Creates Set with unique values
        Set set = new HashSet();
        for (int firstArrayItem : firstArray) {
            for (int secondArrayItem : secondArray) {
                if (firstArrayItem == secondArrayItem) {
                    set.add(firstArrayItem);
                }
            }
        }

        ArrayList<Integer> list = new ArrayList<>();
        list.addAll(set);
        int[] array = convertListToArray(list);
        System.out.println("Result array with unique values:");
        printArray(array);
    }

    private static void createArrayWithUniqueValues() {
        int[] firstArray = generateRandomArray(5, 20);
        int[] secondArray = generateRandomArray(5, 10);

        // Prints first Array
        System.out.println("First array:");
        printArray(firstArray);
        // Prints second Array
        System.out.println("Second array:");
        printArray(secondArray);

        // Make the two lists
        List<Integer> firstList = convertArrayToList(firstArray);
        List<Integer> secondList = convertArrayToList(secondArray);
        // Created union Set of two Lists
        Set<Integer> union = new HashSet<>(firstList);
        union.addAll(secondList);
        // Create intersection of two lists
        Set<Integer> intersection = new HashSet<>(firstList);
        intersection.retainAll(secondList);
        // Removes intersection for the Union Set
        union.removeAll(intersection);
        // Creates new array which is based on union Set with unique values
        int[] result = new int[union.size()];
        int index = 0;
        for (Integer item : union) {
            result[index] = item;
            index++;
        }
        // Print the result
        System.out.println("Result array:");
        printArray(result);
    }

    private static void removeDuplicatesInArray() {
        int[] array = generateRandomArray(10, 5);
        System.out.println("Initial array:");
        printArray(array);

        HashMap<Integer, Integer> map = new HashMap<>();
        // Counts numbers repeating and put them to map
        for (int item : array) {
            if (map.containsKey(item)) {
                map.put(item, map.get(item) + 1);
            } else {
                map.put(item, 1);
            }
        }
        // Creates List without duplicates(values which are repeated more than 2 times)
        ArrayList<Integer> resultList = new ArrayList<>();
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            Integer key = entry.getKey();
            Integer value = entry.getValue();
            if (value <= 2) {
                resultList.add(key);
            }
        }
        int[] result = convertListToArray(resultList);
        // Prints Map with counts
        System.out.println("Map with counts:");
        System.out.println(map);
        // Prints result array
        System.out.println("Result array with unique values:");
        printArray(result);
    }

    private static List<Integer> convertArrayToList(int[] array) {
        List<Integer> list = new ArrayList<>();
        for (int item : array) {
            list.add(item);
        }
        return list;
    }

    private static int[] convertListToArray(List<Integer> list) {
        int[] array = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            array[i] = list.get(i);
        }
        return array;
    }

    private static void printArray(int[] array) {
        for (int item : array) {
            System.out.print(" " + item + " ");
        }
    }

    private static int[] generateRandomArray(int arrayLength, int upperBound) {
        int[] array = new int[arrayLength];
        Random random = new Random();
        for (int i = 0; i < arrayLength; i++) {
            array[i] = random.nextInt(upperBound);
        }

        return array;
    }
}
